/*This programs helps to find the number of a consecutive ones by converting decimal to binary representation*/
package learning;

import java.util.*;

public class Learning_BitManipulation {
	public static void main(String[] args) {
		int count=0;
		int max=0;
	    Scanner scan = new Scanner(System.in) ;
	    int n = scan.nextInt() ;

	    while(n>0)
	    {
	    	if(n%2==1)
	    	{
	    		count+=1;
	    		n=n/2;
	    		if(max<=count)
	    		{
	    		max=count;
	    			
	    		}
	    		
	    	}
	    	else{
	    		count = 0;
	    		n=n/2;
	    	}
	    	
	    }
	   
	    }
	
	
}

